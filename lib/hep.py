#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import scapy.all as sa


HEP_PACKET_HEADER = 6
HEP_CHUNK_HEADER  = 6
HEP_CHUNK_TYPES   = {
    1:  "IP Protocol Family",
    2:  "IP Protocol ID",
    3:  "IPv4 Source Address",
    4:  "IPv4 Destination Address",
    5:  "IPv6 Source Address",
    6:  "IPv6 Destination Address",
    7:  "Protocol Source Port",
    8:  "Protocol Destination Port",
    9:  "Timestamp",
    10: "Timestamp Microseconds Offset",
    11: "Protocol Type",
    12: "Capture Agent ID",
    14: "Authenticate Key",
    15: "Captured Packet Payload",
    17: "Internal Correlation ID"
    }

class HEP3ChunkIPProtoFam(sa.Packet):
    name = "HEP3ChunkIPProtoFam"
    fields_desc = [
        sa.ByteEnumField("value", 2, {2:"IPv4", 10:"IPv6"}),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkIPProtoID(sa.Packet):
    name = "HEP3ChunkIPProtoID"
    fields_desc = [
        sa.ByteEnumField("value", 2, sa.IP_PROTOS),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkIPSrc(sa.Packet):
    name = "HEP3ChunkIPSrc"
    fields_desc = [
        sa.IPField("value", None),
        #sa.StrLenField("value", None, length_from=lambda p: p.underlayer.chunk_length - HEP_CHUNK_HEADER),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkIPDst(sa.Packet):
    name = "HEP3ChunkIPDst"
    fields_desc = [
        sa.IPField("value", None),
        # strlenfield so it can be inserted
        #sa.StrLenField("value", None, length_from=lambda p: p.underlayer.chunk_length - HEP_CHUNK_HEADER),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkIP6Src(sa.Packet):
    name = "HEP3ChunkIP6Src"
    fields_desc = [
        sa.IPField("value", None),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkIP6Dst(sa.Packet):
    name = "HEP3ChunkIP6Dst"
    fields_desc = [
        sa.IPField("value", None),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkSport(sa.Packet):
    name = "HEP3ChunkSport"
    fields_desc = [
        sa.ShortField("value", None),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkDport(sa.Packet):
    name = "HEP3ChunkDport"
    fields_desc = [
        sa.ShortField("value", None),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkTime(sa.Packet):
    name = "HEP3ChunkTime"
    fields_desc = [
        sa.IntField("value", None),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkTimeMS(sa.Packet):
    name = "HEP3ChunkTimeMS"
    fields_desc = [
        sa.IntField("value", None),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkPType(sa.Packet):
    name = "HEP3ChunkPType"
    fields_desc = [
        sa.ByteEnumField(
            "value",
            None,
            {
                1   : "SIP",
                2   : "XMPP",
                3   : "SDP",
                4   : "RTP",
                5   : "JSON/RPC",
                8   : "M2UA",
                10  : "IAX",
                13  : "M2PA",
                20  : "JSON/webRTC",
                32  : "JSON/QOS/32",
                34  : "JSON/QOS/34",
                35  : "MOS",
                99  : "JSON/QOS/99",
                100 : "LOG",
            }
        ),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkCaptID(sa.Packet):
    name = "HEP3ChunkCaptID"
    fields_desc = [
        sa.IntField("value", None),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkAuthK(sa.Packet):
    name = "HEP3ChunkAuthK"
    fields_desc = [
        sa.StrLenField("value", "", length_from=lambda p: p.underlayer.chunk_length - HEP_CHUNK_HEADER if p.underlayer else len("HEP3ChunkAuthK")),
    ]

    def extract_padding(self, s):
        return "", s

class HEP3ChunkCPPay(sa.Packet):
    name = "HEP3ChunkCPPay"
    fields_desc = [
        sa.StrLenField("value", "", length_from=lambda p: p.underlayer.chunk_length - HEP_CHUNK_HEADER if p.underlayer else len("HEP3ChunkCPPay")),
    ]

    def extract_padding(self, s):
        return "", s

    def post_build(self, p, pay):
        return p+pay


class HEP3ChunkICID(sa.Packet):
    name = "HEP3ChunkICID"
    fields_desc = [
        sa.StrLenField("value", "", length_from=lambda p: p.underlayer.chunk_length - HEP_CHUNK_HEADER if p.underlayer else len("HEP3ChunkICID")),
    ]

    def extract_padding(self, s):
        return "", s

    def post_build(self, p, pay):
        return p+pay


class HEP3Chunk(sa.Packet):
    name = "HEP3Chunk"
    fields_desc = [
        sa.ShortField("chunk_vendor_id", 0),
        sa.ShortEnumField("chunk_type_id", 17, HEP_CHUNK_TYPES),
        sa.ShortField("chunk_length", None),
    ]

    def extract_padding(self, s):
        l = self.chunk_length - HEP_CHUNK_HEADER
        return s[:l], s[l:]

    def post_build(self, p, pay):
        # p is the current layer
        # pay is payload
        if self.chunk_length is None and pay:
            l = len(pay)
            # take first 4 bytes and append 2 bytes of length
            p = p[:4] + (l + HEP_CHUNK_HEADER).to_bytes(2, byteorder='big')
        return p+pay


class HEP3(sa.Packet):
    name = "HEP3"
    fields_desc = [
        sa.StrFixedLenField("hep_proto_id", "HEP3", 4),
        sa.ShortField("total_length", None),
        #sa.BitFieldLenField("total_length", None, 2, length_of="HEP3"),
        sa.PacketListField("chunks", None, HEP3Chunk, length_from=lambda p: p.total_length - HEP_PACKET_HEADER),
    ]

    def extract_padding(self, s):
        l = self.total_length - HEP_PACKET_HEADER
        #return "", s
        return s[:l], s[l:]

    def post_build(self, p, pay):
        # p is the current layer
        # pay is payload
        if self.total_length is None:
            l = len(p)
            if pay:
                # this is used in case of packet crafting when scapy does not know the payload is to be parsed as
                # packet list. Hence the total length needs to be calculated from payload as well.
                p = p[:4] + (l+len(pay)).to_bytes(2, byteorder='big') + p[6:]
            else:
                # this is used in case of length updates when payload is not changed, since payload effectively translates
                # to packet list and hence in scapy terminology it is empty
                p = p[:4] + (l).to_bytes(2, byteorder='big') + p[6:] # HEP3 header, 2B length, rest of the packet
        return p+pay

sa.bind_layers(sa.UDP,    HEP3,                sport=9060)
sa.bind_layers(sa.UDP,    HEP3,                dport=9060)
sa.bind_layers(HEP3,      HEP3Chunk,           )
sa.bind_layers(HEP3Chunk, HEP3ChunkIPProtoFam, chunk_type_id=1)
sa.bind_layers(HEP3Chunk, HEP3ChunkIPProtoID,  chunk_type_id=2)
sa.bind_layers(HEP3Chunk, HEP3ChunkIPSrc,      chunk_type_id=3)
sa.bind_layers(HEP3Chunk, HEP3ChunkIPDst,      chunk_type_id=4)
sa.bind_layers(HEP3Chunk, HEP3ChunkIP6Src,     chunk_type_id=5)
sa.bind_layers(HEP3Chunk, HEP3ChunkIP6Dst,     chunk_type_id=6)
sa.bind_layers(HEP3Chunk, HEP3ChunkSport,      chunk_type_id=7)
sa.bind_layers(HEP3Chunk, HEP3ChunkDport,      chunk_type_id=8)
sa.bind_layers(HEP3Chunk, HEP3ChunkTime,       chunk_type_id=9)
sa.bind_layers(HEP3Chunk, HEP3ChunkTimeMS,     chunk_type_id=10)
sa.bind_layers(HEP3Chunk, HEP3ChunkPType,      chunk_type_id=11)
sa.bind_layers(HEP3Chunk, HEP3ChunkCaptID,     chunk_type_id=12)
sa.bind_layers(HEP3Chunk, HEP3ChunkAuthK,      chunk_type_id=14)
sa.bind_layers(HEP3Chunk, HEP3ChunkCPPay,      chunk_type_id=15)
sa.bind_layers(HEP3Chunk, HEP3ChunkICID,       chunk_type_id=17)
