#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
HEP analysis and anonymization tool based on Python3 Scapy library.

It is meant to work with systemd that handles all the daemonization
stuff.
"""



#******************** Imports section **********************
import scapy.all            as sa
import lib.hep              as hep
import logging              as lo
import logging.config       as lc
from hashlib import blake2b as b2b
import datetime             as dt
import re
import random
#***************** End of Imports section ******************

#********************** Info section ***********************
__author__     = "J.Rozhon, F.Rezac"
__copyright__  = "Copyright 2018, CESNET z.s.p.o."
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = "J.Rozhon"
__email__      = "jan.rozhon@gmail.com"
__status__     = "Development"
#******************** End of Info section ******************

#************************ Constants ************************
LOCAL_IP       = "10.2.0.227"
REMOTE_IP      = "10.2.0.98"
HEP_LOCAL_PORT = 9060
B2B_BYTE_SIZE  = 4        # 4 bytes for IP compatibility
LOGGER_CONFIG  = dict(
    version = 1,
    formatters = {
        'f': {'format':
              '%(asctime)s [%(levelname)s] %(message)s'}
        },
    handlers = {
        'h': {'class': 'logging.StreamHandler',
              'formatter': 'f',
              'level': lo.DEBUG}
        },
    root = {
        'handlers': ['h'],
        'level': lo.DEBUG,
        },
)

RE_TAG       = 'tag=(.*)[;,\r\n]'
RE_BRANCH    = 'branch=(.*)[;,\r\n]'
RE_TO_REMOVE = dict(
    P_HEADER  = 'P-.*?\r\n',
    UA_HEADER = 'User-Agent.*?\r\n',
    )
#********************* End of Constants ********************
def random_ipv4():
    return '.'.join(str(random.randint(0,255)) for _ in range(4))


def manage_anonymization(k, cls, s, u=False):
    """
    u - should the accessed be updated?
    k - topmost key in table
    ####t - anon table
    cls - class of the given entity
    s - string representation of the entity
    """
    # select the table
    if k in anon_table.keys():
        t = anon_table[k]
    else:
        anon_table[k] = {}
        t = anon_table[k]
    if u:
        t["accessed"] = dt.datetime.now()
    # populate the table
    if cls == "ipv4":
        if s in t.keys():
            return t[s]
        else:
            t[s] = random_ipv4()
            return t[s]
    elif cls == "ipv6":
        pass
    elif cls == "str":
        if s in t.keys():
            return t[s]
        else:
            h = b2b(s.encode("utf-8"), digest_size=B2B_BYTE_SIZE).hexdigest()
            t[s] = h
            print(type(h), h)
            return h
    else:
        raise Exception("Unknown class.")

def parse_sip(s):
    """
    s is string all the time.
    Parse the sensitive information and call anon function.
    """

    # first parse call id since it is to be used in HEP as well
    # CID is used as topmost key in anon_table dictionary
    k = re.search("Call-ID: ?(.*?)\r\n", s).group(1)
    s.replace(k, manage_anonymization(k, "str", k, u=True))

    # hash the tags
    for t in re.findall(RE_TAG, s):
        s.replace(t,manage_anonymization(k, "str", t))



    for _, r in RE_TO_REMOVE.keys():
        matches = None
        matches = re.findall(r, s)
        if matches:
            for m in matches:
                s.replace(m, "")



    # then, look for ip addresses in the message
    ip_add = re.findall(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", s)
    for ip in ip_add:
        r = anon(k, "ipv4", ip)
        s = s.replace(ip, r)

    return s

def callback(packet):
    packet["IP"].src = LOCAL_IP
    packet["IP"].dst = REMOTE_IP

    # delete checksums and lengths so they are recalculated
    del packet["IP"].chksum
    del packet["UDP"].chksum
    del packet["IP"].len
    del packet["UDP"].len
    del packet["HEP3"].total_length

    chunk_mapping = {}
    for chunk in packet["HEP3"].chunks:
        chunk_mapping[chunk.chunk_type_id] = chunk

    # handle sip payload chunk first, since the data in SIP are present in HEP as well
    sc = chunk_mapping[15]
    del sc.chunk_length # delete chunk length so it is recalculated

    sip_bytes = sc["HEP3ChunkCPPay"].value              # assign sip bytes to variable for easy work
    sip_pay = sip_bytes.decode("utf-8")                 # convert to str
    sip_pay = parse_sip(sip_pay)                     # parse and anonymize SIP
    new_sip_bytes = sip_pay.encode("utf-8")
    sc.payload = hep.HEP3ChunkCPPay(value = sip_pay)



    # first, take internal correlation ID (callid) for mapping
    k = chunk_mapping[17]["HEP3ChunkICID"].value.decode("utf-8")

    # then hide source and dest. addresses
    chunk_mapping[3]["HEP3ChunkIPSrc"].value = anon(k, "ipv4", chunk_mapping[3]["HEP3ChunkIPSrc"].value.encode("utf8"))
    chunk_mapping[4]["HEP3ChunkIPDst"].value = anon(k, "ipv4", chunk_mapping[4]["HEP3ChunkIPDst"].value.encode("utf8"))


    sa.send(packet["IP"], verbose=0)
    l.info("Packet sent with payload: {}".format(sip_pay.replace("\r\n", "#")))
    return

def main(at=None):
    lc.dictConfig(LOGGER_CONFIG)
    l = lo.getLogger()
    # global storage for mappings
    global anon_table
    if at:
        anon_table = at
    else:
        anon_table = {}
    sa.sniff(filter="udp port {} and dst {}".format(HEP_LOCAL_PORT, LOCAL_IP), prn=callback)


if __name__ == '__main__':
    main()
