#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
HEP analysis and anonymization tool based on Python3 Scapy library.

This process only takes care of anonymization table so it does not
grow infinitely.

It deletes the call-ids after the specified amount of time to handle
the lost SIP packets.
"""

#******************** Imports section **********************
import logging              as lo
import logging.config       as lc
from hashlib import blake2b as b2b
import datetime             as dt
import time
#***************** End of Imports section ******************

#********************** Info section ***********************
__author__     = "J.Rozhon, F.Rezac"
__copyright__  = "Copyright 2018, CESNET z.s.p.o."
__license__    = "GPL"
__version__    = "0.0.1"
__maintainer__ = "J.Rozhon"
__email__      = "jan.rozhon@gmail.com"
__status__     = "Development"
#******************** End of Info section ******************

#************************ Constants ************************
TIME_TO_SLEEP = 60
TIME_TO_KEEP  = 7200
LOGGER_CONFIG  = dict(
    version = 1,
    formatters = {
        'f': {'format':
              '%(asctime)s [%(levelname)s] %(message)s'}
        },
    handlers = {
        'h': {'class': 'logging.StreamHandler',
              'formatter': 'f',
              'level': lo.DEBUG}
        },
    root = {
        'handlers': ['h'],
        'level': lo.DEBUG,
        },
)
#********************* End of Constants ********************
def main(at=None):
    lc.dictConfig(LOGGER_CONFIG)
    l = lo.getLogger()
    # global storage for mappings
    global anon_table
    if at:
        anon_table = at
    else:
        raise Exception("No anonymization table specified.")

    # main loop here
    while True:
        now = dt.datetime.now()
        for k,v in anon_table.items():
            if k != 'created':
                if now - v['accessed'] >= TIME_TO_KEEP:
                    del anon_table[k]
        print(anon_table)
        time.sleep(TIME_TO_SLEEP)

if __name__ == '__main__':
    main()
