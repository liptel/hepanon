#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from multiprocessing import Process, Manager
import lib.hd               as hd
import lib.consistencyd     as cd
import datetime             as dt
if __name__ == '__main__':
    manager = Manager()
    # anonymization table
    at = manager.dict()
    at['created'] = dt.datetime.now()
    
    p1 = Process(target=hd.main, args=(at,))
    p2 = Process(target=cd.main, args=(at,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
